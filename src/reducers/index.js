import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { shopReducers } from '../shop/reducers'
export const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  shop: shopReducers
});
