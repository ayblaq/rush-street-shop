import React from "react";
import { Button, Header, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Slider } from "../../../common/components";
import { sliderImages } from "../../../common";
import "./Home.css";

const Home = () => {
  return (
    <div className={"full-container"}>
      <Slider slides={sliderImages} size={sliderImages.length - 1}></Slider>
      <div className={"home-title-container"}>
        <Header as="h1" icon>
          <Icon name="shopping bag" size="mini" />
          Your number one Online Thrift Store.
          <Header.Subheader>
            Trusted by over 100,000 customers worldwide.
          </Header.Subheader>
          <Button color="teal" as={Link} to={"/home"}>
            Start Shopping Now
          </Button>
        </Header>
      </div>
    </div>
  );
};

export default Home;
