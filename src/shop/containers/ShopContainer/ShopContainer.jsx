import React, { Suspense } from "react";
import "./ShopContainer.scss";
import { Banner, PageLoader } from "../../../common/components";

const ShopContainer = () => {
  const mini = "Most trusted online THRIFT store.";
  const title = "All orders will continue shipping as normal.";

  const Shop = React.lazy(() => import("../../components/Shop/Shop"));

  return (
    <div className={"full-container"}>
      <Banner mini={mini} title={title}></Banner>
      <div>
        <Suspense fallback={<PageLoader></PageLoader>}>
          <Shop></Shop>
        </Suspense>
      </div>
    </div>
  );
};

export default ShopContainer;
