import React, { Suspense } from "react";
import "./StoreContainer.scss";
import { Banner, PageLoader } from "../../../common/components";

const StoreContainer = () => {
  const Store = React.lazy(() => import("../../components/Store/Store"));

  return (
    <div className={"full-container"}>
      <Banner
        mini={"Thrifted Stores"}
        title={"Search through popular products and stores"}
      ></Banner>
      <div>
        <Suspense fallback={<PageLoader></PageLoader>}>
          <Store></Store>
        </Suspense>
      </div>
    </div>
  );
};

export default StoreContainer;
