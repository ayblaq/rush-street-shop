import { UPDATE_CART, ADD_ITEM_CART } from "../actions/CartActions";

export const cartReducer = (
  state = {
    carts: [],
  },
  action
) => {
  switch (action.type) {
    case ADD_ITEM_CART:
      return {
        ...state,
        carts: [...state.carts, action.payload],
      };
    case UPDATE_CART:
      return {
        ...state,
        carts: [...action.payload],
      };
    default:
      return state;
  }
};
