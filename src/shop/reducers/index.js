import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { shopReducer } from './ShopReducer';
import { cartReducer } from './CartReducer'

const persistConfig = {
  key: 'products',
  storage: storage,
};

const shopReducers = (state = {}, action) => {
  return {
    products: shopReducer(state.products, action),
    carts: cartReducer(state.carts, action)
  }
};

const reducer = persistReducer(persistConfig, shopReducers);

export { reducer as shopReducers }
