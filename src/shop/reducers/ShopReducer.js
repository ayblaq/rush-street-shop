import {
    UPDATE_PRODUCTS
} from '../actions/ShopActions'

export const shopReducer = (state = {
    products: {},
}, action) => {
    switch (action.type) {
        case UPDATE_PRODUCTS:
            return {
                ...state,
                products: {
                    ...action.payload,
                }
            };
        default:
            return state
    }
}