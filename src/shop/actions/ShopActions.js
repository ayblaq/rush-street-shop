import { mockProduct } from "../api/api";

export const UPDATE_PRODUCTS = "UPDATE_PRODUCTS";

export const updateProducts = (products) => ({
  type: UPDATE_PRODUCTS,
  payload: products,
});

export const fetchProducts = (products) => (dispatch) => {
  if (Object.keys(products).length === 0) {
    dispatch(updateProducts(mockProduct));
  }
};
