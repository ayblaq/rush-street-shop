export const ADD_ITEM_CART = "ADD_ITEM_CART";
export const UPDATE_CART = "UPDATE_CART";

export const addItemCart = (cart) => ({
  type: ADD_ITEM_CART,
  payload: cart,
});

export const updateCart = (carts) => ({
  type: UPDATE_CART,
  payload: carts,
});
