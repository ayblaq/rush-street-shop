import React from "react";
import "./ShopTable.scss";
import { Table, } from "semantic-ui-react";
import  dateformat from "dateformat";

const ShopTable = (props) => {
  const { columns, rows } = props;

  const formatData = (row, type, col) => {
    if (type === "date") {
      return dateformat(row[col], "dddd, mmmm dS, yyyy, h:MM")
    } else if (type === "link") {
        return <a  rel={"noopener noreferrer"} target="_blank" href={row['link']}>{row[col]}</a>
    }

    return row[col]
  }

  return (
      <Table celled>
        <Table.Header>
          <Table.Row>
            {columns.map((value, key) => {
              return <Table.HeaderCell key={key}>{value.name}</Table.HeaderCell>;
            })}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {rows.map((row, key) => {
            return (
              <Table.Row key={key}>
                {columns.map((col, index) => {
                  return <Table.Cell collapsing key={index}>{formatData(row, col.type, col.title)}</Table.Cell>;
                })}
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
  );
};

export default ShopTable;
