import React, { useState } from "react";
import "./ShopCard.css";
import { Card, Image, Button } from "semantic-ui-react";

const ShopCard = (props) => {
  const { data, updateFunc } = props;
  
  const [item, setItem] = useState({ quantity: 0 });

  const increment = (e) => {
    if (item.quantity < data.quantity) {
      setItem({ quantity: item.quantity + 1 });
    }
  };

  const decrement = (e) => {
    if (item.quantity > 0) {
      setItem({ quantity: item.quantity - 1 });
    }
  };

  const clearCounter = (e) => {
    if (item.quantity > 0) {
      updateFunc(e, data.id, item.quantity);
      setItem({ quantity: 0 });
    }
  };

  return (
    <Card>
      <Image src={data.image} />
      <Card.Content>
        <Card.Header>{data.name}</Card.Header>
        <Card.Meta>{`${data.price} EUR`}</Card.Meta>
        <Card.Description>{data.desc}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className={"flex-container"}>
          <div className={"half-box"}>
            <ul className="dd-list">
              <li className="dd-list-item">
                <Button
                  floated="right"
                  size="mini"
                  icon="plus"
                  color="blue"
                  circular
                  onClick={(e) => {
                    increment(e);
                  }}
                />
                <span className="right ticket-counter">{item.quantity}</span>
                <Button
                  floated="right"
                  size="mini"
                  icon="minus"
                  color="blue"
                  circular
                  onClick={(e) => decrement(e)}
                />
              </li>
            </ul>
          </div>
          <div className={"half-box"}>
            <Button
              basic
              color="green"
              floated="right"
              content="BUY"
              onClick={(e) => clearCounter(e)}
            />
          </div>
        </div>
      </Card.Content>
    </Card>
  );
};

export default ShopCard;
