import React, { useEffect } from "react";
import { Card } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { ShopCard } from "../../components";
import "./ShopList.scss";
import { PageLoader } from "../../../common/components";
import { fetchProducts, updateProducts, addItemCart } from "../../actions/";

const ShopList = () => {
  const {
    products: { products },
  } = useSelector((state) => state.shop);
  
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(fetchProducts(products));
  });

  const updateProduct = (e, id, quantity) => {
      if (products[id]) {
        products[id] = { ...products[id], quantity: products[id].quantity - quantity }
        const cart = {...products[id], quantity: quantity}
        dispatch(updateProducts(products))
        dispatch(addItemCart(cart))
      }
  }

  if (Object.keys(products).length > 0) {
    return (
      <Card.Group>
        {Object.keys(products).map((key, value) => {
          return <ShopCard data={products[key]} key={value} updateFunc={updateProduct}></ShopCard>;
        })}
      </Card.Group>
    );
  } else {
    return <PageLoader></PageLoader>;
  }
};

export default ShopList;
