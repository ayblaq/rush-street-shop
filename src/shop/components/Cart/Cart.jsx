import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Segment, Table, Icon } from "semantic-ui-react";
import { updateCart, updateProducts } from "../../actions/";
import "./Cart.scss";

const Cart = () => {
  const {
    carts: { carts },
    products: { products },
  } = useSelector((state) => state.shop);

  const dispatch = useDispatch();

  const items = carts.map((item, key) => (
    <Table.Row key={key}>
      <Table.Cell>
        {item.name} ({item.price * item.quantity} EUR)
      </Table.Cell>
      <Table.Cell textAlign={"right"}>
        {item.quantity}
        <Icon
          name="cancel"
          color="red"
          onClick={(e) => removeItem(e, key)}
        ></Icon>
      </Table.Cell>
    </Table.Row>
  ));

  let removeItem = (e, index) => {
    e.preventDefault();
    const product = carts[index];
    let new_carts = carts.filter(function (item, key) {
      return key !== index;
    });
    dispatch(updateCart(new_carts));
    if (products[product.id]) {
      products[product.id] = {
        ...products[product.id],
        quantity: products[product.id].quantity + product.quantity,
      };
      dispatch(updateProducts(products));
    }
  };

  let calcTotal = () => {
    return carts.reduce((acc, obj) => {
      return obj.quantity * obj.price + acc;
    }, 0);
  };

  return (
    <Segment raised={true} className={"cart-card"}>
      <h1>Cart</h1>
      <Table size={"large"} basic={"very"} unstackable>
        <Table.Header>
          <Table.Row></Table.Row>
        </Table.Header>
        <Table.Body>{items}</Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.Cell>Total</Table.Cell>
            <Table.Cell textAlign={"right"}>
              <h4 className="ui header"> {calcTotal()} EUR</h4>
            </Table.Cell>
          </Table.Row>
        </Table.Footer>
      </Table>
    </Segment>
  );
};

export default Cart;
