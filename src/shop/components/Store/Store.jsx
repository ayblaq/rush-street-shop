import React, { useState } from "react";
import { ShopTable } from "../../components";
import { mockStore, mockHeader } from "../../api/api";
import { Form } from "semantic-ui-react";
import dateformat from "dateformat";

import "./Store.scss";

const Store = () => {
  const [filter, setFilter] = useState(mockStore);
  const [search, setValue] = useState({ word: "" });

  const searchField = () => {
    let filtered = mockStore.filter((item) => {
      return (
        item.name.toLowerCase().includes(search.word.toLowerCase()) ||
        item.store.toLowerCase().includes(search.word.toLowerCase()) ||
        dateformat(item.created_at, "dddd, mmmm dS, yyyy, h:MM")
          .toLowerCase()
          .includes(search.word.toLowerCase())
      );
    });
    setFilter([...filtered]);
  };

  const handleChange = (e) => {
    if (e.target.value.length === 0) {
      setFilter(mockStore);
    }
    setValue({ word: e.target.value });
  };

  return (
    <div className={"main-container"}>
      <Form onSubmit={() => searchField()}>
        <Form.Group>
          <Form.Input
            placeholder="search"
            name="search"
            value={search.word}
            onChange={handleChange}
          />
          <Form.Button content="Search" />
        </Form.Group>
      </Form>
      <ShopTable
        rows={filter}
        columns={mockHeader}
      ></ShopTable>
    </div>
  );
};

export default Store;
