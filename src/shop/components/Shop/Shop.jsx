import React from "react";
import { ShopList, Cart } from "../../components";
import "./Shop.scss";

const Shop = () => {
  return (
    <div className={"flex-container main-container"}>
      <div className={"qtr-box-inv"}>
        <Cart></Cart>
      </div>
      <div className={"qtr-box"}>
        <ShopList></ShopList>
      </div>
    </div>
  );
};

export default Shop;
