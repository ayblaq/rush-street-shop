import React from "react";
import { Icon, Label, Menu } from "semantic-ui-react";
import { useSelector } from "react-redux";
import "./CartMenu.scss";

const CartMenu = () => {
  const {
    carts: { carts },
  } = useSelector((state) => state.shop);

  return (
    <Menu.Menu position="right">
      <Menu.Item>
        <Icon name="shopping cart"/>
        <Label color="red">
          {carts.length}
        </Label>
      </Menu.Item>
    </Menu.Menu>
  );
};

export default CartMenu;
