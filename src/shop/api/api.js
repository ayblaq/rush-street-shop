import nike1 from "../../images/nike-shoe-1.jpg";
import nike2 from "../../images/nike-shoe-2.jpg";
import jordan from "../../images/jordan-shoe-1.jpg";
import yeezy1 from "../../images/yeezy-shoe-1.jpg";
import thrift from "../../images/thrift-shoe-1.jpg";
import vans from "../../images/vans-shoe-1.jpg";

export const mockStore = [
  {
    id: "history_1#",
    name: "Unchecked kicks",
    store: "New balance",
    link: "https://www.newbalance.com/",
    created_at: "2020-08-27T08:00:00.000Z",
  },
  {
    id: "history_2#",
    name: "Air Jordan Dior",
    store: "Dior",
    link: "https://www.dior.com/en_us/mens-fashion/air-dior",
    created_at: "2020-09-27T08:00:00.000Z",
  },
  {
    id: "history_3#",
    name: "Air Jordan Dior",
    store: "Dior",
    link: "https://www.dior.com/en_us/mens-fashion/air-dior",
    created_at: "2020-09-27T08:00:00.000Z",
  },
  {
    id: "history_4#",
    name: "Yeezy",
    store: "Adidas",
    link: "https://adidas.com/us",
    created_at: "2020-09-27T08:00:00.000Z",
  },
];

export const mockProduct = {
  "y#121": {
    id: "y#121",
    name: "Nike Retro",
    price: 50,
    image: nike1,
    quantity: 10,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ",
  },
  "y#122": {
    id: "y#122",
    name: "Nike Fling",
    price: 50,
    image: nike2,
    quantity: 10,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ",
  },
  "y#123": {
    id: "y#123",
    name: "Air Jordan",
    price: 20,
    image: jordan,
    quantity: 10,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ",
  },
  "y#124": {
    id: "y#124",
    name: "Yeezy",
    price: 30,
    image: yeezy1,
    quantity: 10,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ",
  },
  "y#125": {
    id: "y#125",
    name: "Unchecked",
    price: 40,
    image: thrift,
    quantity: 10,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ",
  },
  "y#126": {
    id: "y#126",
    name: "Vans",
    price: 10,
    image: vans,
    quantity: 10,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ",
  },
};

export const mockHeader = [
  {
    name: "Product name",
    title: "name",
    type: "string",
  },
  {
    name: "Store",
    title: "store",
    type: "link",
  },
  {
    name: "Purchase Date",
    title: "created_at",
    type: "date",
  },
];
