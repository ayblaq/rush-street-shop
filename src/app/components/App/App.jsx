import React from "react";
import { withRouter } from "react-router";
import { SiteHeader, Footer } from "../../../common/components";
import "./App.scss";

const App = (props) => {
  let { children, location } = props;
 
  return (
    <div className="page-layout">
      <SiteHeader active={location.pathname}></SiteHeader>
      <main>
        {children}
        <div className="main-content">
        </div>
        <Footer></Footer>
      </main>
    </div>
  );
};

export default withRouter(App);
