import React from "react";
import { Route, Redirect, Switch } from "react-router";
import { createBrowserHistory } from "history";
import { App } from "../app/components";
import { Home } from "../home/components";
import { ShopContainer, StoreContainer } from "../shop/containers";

export const history = getHistory();

export const appRouting = [
  {
    path: "/",
    name: "Home",
    exact: true,
    tag: Route,
    component: Home,
  },
  {
    path: "/home",
    name: "Shop",
    exact: true,
    tag: Route,
    component: ShopContainer,
  },
  {
    path: "/store",
    name: "Store History",
    exact: true,
    tag: Route,
    component: StoreContainer,
  },
];

export const Routing = () => {
  let routes = appRouting.filter((a) => a.tag || a.component);
  let routesRendered = routes.map((a, i) => {
    let Tag = a.tag;
    let { path, exact, strict, name, component } = a;
    let b = { path, exact, strict, name, component };

    return <Tag key={i} {...b} />;
  });
  return (
    <App>
      <Switch>
        {routesRendered}
        <Redirect to="/" />
      </Switch>
    </App>
  );
};

function getHistory() {
  const basename = process.env.BUILD_DEMO ? "/react-semantic.ui-starter" : "";

  return createBrowserHistory({ basename });
}
