import React from "react";
import { Link } from 'react-router-dom';
import { Container, Menu } from "semantic-ui-react";
import { appRouting } from "../../../routing/Routing";
import { CartMenu } from "../../../shop/components"

const SiteHeader = (props) => {
  const { active } = props;
  return (
    <Menu fixed="top" inverted>
      <Container>
        {appRouting.map((value, index) => {
          return (
            <Menu.Item
              key={index}
              as={Link}
              to={value.path}
              name={value.name}
              active={active === value.path}
            />
          );
        })}
            <CartMenu></CartMenu>
        </Container>
    </Menu>
  );
};

export default SiteHeader;
