import React from "react";
import { Grid, Header, Icon } from "semantic-ui-react";
import "./Footer.scss";

const Footer = () => {
  return (
    <footer>
      <div className="footer-inner">
        <Grid>
          <Grid.Row>
            <Grid.Column width={12}>
              <a href="https://rsi.com">
                <Header as="h3" inverted>
                  <Icon name="github" />
                  <Header.Content>
                    Rush Street Shop
                    <Header.Subheader>Created with Love.</Header.Subheader>
                  </Header.Content>
                </Header>
              </a>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </footer>
  );
};

export default Footer;
