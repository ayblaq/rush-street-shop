import React from "react";
import "./Banner.scss";
import { Header } from "semantic-ui-react";

const Banner = (props) => {
  const { mini, title } = props;

  return (
    <div>
      <div className={"banner"}>
        <div className={"home-title-container"}>
          <Header as="h1" className={"font-w"} icon>
            {title}
            <Header.Subheader className={"font-w"}>{mini}</Header.Subheader>
          </Header>
        </div>
      </div>
    </div>
  );
};

export default Banner;
