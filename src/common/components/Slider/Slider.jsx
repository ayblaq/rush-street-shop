import React, { useEffect, useState, useRef } from "react";
import "./Slider.scss";

const Slider = (props) => {
  const [counter, setCounter] = useState({ index: 0, timer: 0 });
  const { slides, size, sliderclass } = props;
  const slideImage = useRef(null);

  const sliderClass = sliderclass
    ? `slider ${sliderclass}`
    : `slider big-slider`;

  useEffect(() => {
    const interval = setInterval(() => {
      moveSlider();
    }, counter.timer);

    return () => {
      clearTimeout(interval);
    };
  });

  const moveSlider = () => {
    const position = counter.index < size ? counter.index + 1 : 0;
    slideImage.current.style.backgroundImage = `linear-gradient(
        to right,
        rgba(34, 34, 34, 0.3),
        rgba(68, 68, 68, 0.3)
        ),url(${slides[position].link})`;

    setCounter({ index: position, timer: 5000 });
  };

  return <div className={sliderClass} ref={slideImage}></div>;
};

export default Slider;
