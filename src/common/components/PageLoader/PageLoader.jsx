import React from "react";
import { Loader } from "semantic-ui-react";
import "./PageLoader.scss";

const PageLoader = (props) => {
  const { size } = props;

  return (
    <div className={"main-container"}>
      <Loader
        className={"home-title-container"}
        active
        inline="centered"
        size={size ? size : "medium"}
      >
        Loading
      </Loader>
    </div>
  );
};

export default PageLoader;
