import slider1 from "../images/home_slider_1.jpg";
import slider2 from "../images/home_slider_2.jpg";
import slider3 from "../images/home_slider_3.jpg";

export const sliderImages = [
    {
        link : slider1,
        alt : "chilling"
    },
    {
        link : slider2,
        alt : "chilling"
    },
    {
        link : slider3,
        alt : "chilling"
    }
]